/* router - rutas para cada página
permite activar los templates necesarios para cargar contenido en la página */

window.addEventListener("load", () => {
  const router = new Navigo("/", { hash: true });
  //const render = (content) => (document.querySelector("#contenido").innerHTML = content);

  router
    .on("/", (match) => {
      borrarContenido();
      //setTitulo(3,"titulosSecciones")
      setContenidoCuadernosIni();
      console.log("cuadernos");
      //cambiarBackground(3);
    })
    .on("/cuadernos/cuaderno", (match) => {
      borrarContenido();
    
      setContenidoCuadernosInfo(Object.keys(match.params)[0]);
     // cambiarBackground(3);
    })
    .notFound(() => {
      console.log("404");
     // cambiarBackground(0);
    })
    .resolve();
});
